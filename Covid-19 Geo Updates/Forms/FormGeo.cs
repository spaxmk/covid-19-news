﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Covid_19_Geo_Updates.Forms
{
    public partial class FormGeo : Form
    {
        private CountryData countryData;
        private CountriesList countryList;
        private Countries Countries;
        
        public FormGeo()
        {
            
            LoadMap();
            InitializeComponent();
        }

        private void FormGeo_Load(object sender, EventArgs e)
        {
            //LiveCharts.WinForms.GeoMap geoMap = new LiveCharts.WinForms.GeoMap();
            //Dictionary<string, double> keyValues = new Dictionary<string, double>();
            //keyValues["CN"] = 81285;
            //keyValues["SRB"] = 4585458;
            //keyValues["US"] = 4545;
            //keyValues["PK"] = 7854;
            //keyValues["IT"] = 10000;

            //geoMap.HeatMap = keyValues;
            //geoMap.Source = @"../../res/World.xml";
            //this.Controls.Add(geoMap);
        }

        private void LoadMap()
        {
            LiveCharts.WinForms.GeoMap geoMap = new LiveCharts.WinForms.GeoMap();
            Dictionary<string, double> keyValues = new Dictionary<string, double>();
            keyValues["RS"] = 81285;
            keyValues["US"] = 75066;
            keyValues["IT"] = 74384;
            keyValues["ES"] = 56196;
            keyValues["IR"] = 29046;
            keyValues["FR"] = 25233;
            keyValues["UK"] = 9849;
            keyValues["CA"] = 3579;
            keyValues["PK"] = 1179;
            keyValues["IN"] = 719;

            //for (int i = 0; i < Countries.myList.Count; i++)
            //{

            //    string countryCovid = Countries.myList[i].CountryCode;
            //    int comfirmedCovid = Countries.myList[i].Comfirmed;
            //    keyValues[comfirmedCovid.ToString()] = comfirmedCovid;
            //}

            geoMap.HeatMap = keyValues;

            geoMap.Source = @"../../res/World.xml";
            this.Controls.Add(geoMap);
            geoMap.Dock = DockStyle.Fill;
        }
    }
}
