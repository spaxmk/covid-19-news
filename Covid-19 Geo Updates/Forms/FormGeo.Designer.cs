﻿namespace Covid_19_Geo_Updates.Forms
{
    partial class FormGeo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlGeo = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlGeo
            // 
            this.pnlGeo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGeo.Location = new System.Drawing.Point(0, 0);
            this.pnlGeo.Name = "pnlGeo";
            this.pnlGeo.Size = new System.Drawing.Size(850, 511);
            this.pnlGeo.TabIndex = 0;
            // 
            // FormGeo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 511);
            this.Controls.Add(this.pnlGeo);
            this.Name = "FormGeo";
            this.Text = "FormGeo";
            this.Load += new System.EventHandler(this.FormGeo_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlGeo;
    }
}