﻿namespace Covid_19_Geo_Updates
{
    partial class FormList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlForm = new System.Windows.Forms.Panel();
            this.dgw2 = new System.Windows.Forms.DataGridView();
            this.countryDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provinceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cityCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.latDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comfirmedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deathsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recoveredDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgw1 = new System.Windows.Forms.DataGridView();
            this.countryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slugDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSO2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnGet = new System.Windows.Forms.Button();
            this.LblTime = new System.Windows.Forms.Label();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgw2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgw1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countriesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlForm
            // 
            this.pnlForm.Controls.Add(this.LblTime);
            this.pnlForm.Controls.Add(this.dgw2);
            this.pnlForm.Controls.Add(this.dgw1);
            this.pnlForm.Controls.Add(this.btnGet);
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Location = new System.Drawing.Point(0, 0);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(850, 511);
            this.pnlForm.TabIndex = 0;
            // 
            // dgw2
            // 
            this.dgw2.AutoGenerateColumns = false;
            this.dgw2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.countryDataGridViewTextBoxColumn1,
            this.countryCodeDataGridViewTextBoxColumn,
            this.provinceDataGridViewTextBoxColumn,
            this.cityDataGridViewTextBoxColumn,
            this.cityCodeDataGridViewTextBoxColumn,
            this.latDataGridViewTextBoxColumn,
            this.lonDataGridViewTextBoxColumn,
            this.comfirmedDataGridViewTextBoxColumn,
            this.deathsDataGridViewTextBoxColumn,
            this.recoveredDataGridViewTextBoxColumn,
            this.activeDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn});
            this.dgw2.DataSource = this.countryDataBindingSource;
            this.dgw2.Location = new System.Drawing.Point(136, 106);
            this.dgw2.Name = "dgw2";
            this.dgw2.Size = new System.Drawing.Size(714, 405);
            this.dgw2.TabIndex = 3;
            // 
            // countryDataGridViewTextBoxColumn1
            // 
            this.countryDataGridViewTextBoxColumn1.DataPropertyName = "Country";
            this.countryDataGridViewTextBoxColumn1.HeaderText = "Country";
            this.countryDataGridViewTextBoxColumn1.Name = "countryDataGridViewTextBoxColumn1";
            // 
            // countryCodeDataGridViewTextBoxColumn
            // 
            this.countryCodeDataGridViewTextBoxColumn.DataPropertyName = "CountryCode";
            this.countryCodeDataGridViewTextBoxColumn.HeaderText = "CountryCode";
            this.countryCodeDataGridViewTextBoxColumn.Name = "countryCodeDataGridViewTextBoxColumn";
            // 
            // provinceDataGridViewTextBoxColumn
            // 
            this.provinceDataGridViewTextBoxColumn.DataPropertyName = "Province";
            this.provinceDataGridViewTextBoxColumn.HeaderText = "Province";
            this.provinceDataGridViewTextBoxColumn.Name = "provinceDataGridViewTextBoxColumn";
            // 
            // cityDataGridViewTextBoxColumn
            // 
            this.cityDataGridViewTextBoxColumn.DataPropertyName = "City";
            this.cityDataGridViewTextBoxColumn.HeaderText = "City";
            this.cityDataGridViewTextBoxColumn.Name = "cityDataGridViewTextBoxColumn";
            // 
            // cityCodeDataGridViewTextBoxColumn
            // 
            this.cityCodeDataGridViewTextBoxColumn.DataPropertyName = "CityCode";
            this.cityCodeDataGridViewTextBoxColumn.HeaderText = "CityCode";
            this.cityCodeDataGridViewTextBoxColumn.Name = "cityCodeDataGridViewTextBoxColumn";
            // 
            // latDataGridViewTextBoxColumn
            // 
            this.latDataGridViewTextBoxColumn.DataPropertyName = "Lat";
            this.latDataGridViewTextBoxColumn.HeaderText = "Lat";
            this.latDataGridViewTextBoxColumn.Name = "latDataGridViewTextBoxColumn";
            // 
            // lonDataGridViewTextBoxColumn
            // 
            this.lonDataGridViewTextBoxColumn.DataPropertyName = "Lon";
            this.lonDataGridViewTextBoxColumn.HeaderText = "Lon";
            this.lonDataGridViewTextBoxColumn.Name = "lonDataGridViewTextBoxColumn";
            // 
            // comfirmedDataGridViewTextBoxColumn
            // 
            this.comfirmedDataGridViewTextBoxColumn.DataPropertyName = "Comfirmed";
            this.comfirmedDataGridViewTextBoxColumn.HeaderText = "Comfirmed";
            this.comfirmedDataGridViewTextBoxColumn.Name = "comfirmedDataGridViewTextBoxColumn";
            // 
            // deathsDataGridViewTextBoxColumn
            // 
            this.deathsDataGridViewTextBoxColumn.DataPropertyName = "Deaths";
            this.deathsDataGridViewTextBoxColumn.HeaderText = "Deaths";
            this.deathsDataGridViewTextBoxColumn.Name = "deathsDataGridViewTextBoxColumn";
            // 
            // recoveredDataGridViewTextBoxColumn
            // 
            this.recoveredDataGridViewTextBoxColumn.DataPropertyName = "Recovered";
            this.recoveredDataGridViewTextBoxColumn.HeaderText = "Recovered";
            this.recoveredDataGridViewTextBoxColumn.Name = "recoveredDataGridViewTextBoxColumn";
            // 
            // activeDataGridViewTextBoxColumn
            // 
            this.activeDataGridViewTextBoxColumn.DataPropertyName = "Active";
            this.activeDataGridViewTextBoxColumn.HeaderText = "Active";
            this.activeDataGridViewTextBoxColumn.Name = "activeDataGridViewTextBoxColumn";
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            // 
            // countryDataBindingSource
            // 
            this.countryDataBindingSource.DataSource = typeof(Covid_19_Geo_Updates.CountryData);
            // 
            // dgw1
            // 
            this.dgw1.AutoGenerateColumns = false;
            this.dgw1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.countryDataGridViewTextBoxColumn,
            this.slugDataGridViewTextBoxColumn,
            this.iSO2DataGridViewTextBoxColumn});
            this.dgw1.DataSource = this.countriesBindingSource;
            this.dgw1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dgw1.Location = new System.Drawing.Point(136, 0);
            this.dgw1.Name = "dgw1";
            this.dgw1.Size = new System.Drawing.Size(714, 511);
            this.dgw1.TabIndex = 2;
            // 
            // countryDataGridViewTextBoxColumn
            // 
            this.countryDataGridViewTextBoxColumn.DataPropertyName = "Country";
            this.countryDataGridViewTextBoxColumn.HeaderText = "Country";
            this.countryDataGridViewTextBoxColumn.Name = "countryDataGridViewTextBoxColumn";
            // 
            // slugDataGridViewTextBoxColumn
            // 
            this.slugDataGridViewTextBoxColumn.DataPropertyName = "Slug";
            this.slugDataGridViewTextBoxColumn.HeaderText = "Slug";
            this.slugDataGridViewTextBoxColumn.Name = "slugDataGridViewTextBoxColumn";
            // 
            // iSO2DataGridViewTextBoxColumn
            // 
            this.iSO2DataGridViewTextBoxColumn.DataPropertyName = "ISO2";
            this.iSO2DataGridViewTextBoxColumn.HeaderText = "ISO2";
            this.iSO2DataGridViewTextBoxColumn.Name = "iSO2DataGridViewTextBoxColumn";
            // 
            // countriesBindingSource
            // 
            this.countriesBindingSource.DataSource = typeof(Covid_19_Geo_Updates.Countries);
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(18, 20);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(75, 23);
            this.btnGet.TabIndex = 1;
            this.btnGet.Text = "Get";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // LblTime
            // 
            this.LblTime.AutoSize = true;
            this.LblTime.Location = new System.Drawing.Point(4, 486);
            this.LblTime.Name = "LblTime";
            this.LblTime.Size = new System.Drawing.Size(30, 13);
            this.LblTime.TabIndex = 4;
            this.LblTime.Text = "Date";
            // 
            // FormList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 511);
            this.Controls.Add(this.pnlForm);
            this.Name = "FormList";
            this.Text = "FormList";
            this.pnlForm.ResumeLayout(false);
            this.pnlForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgw2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgw1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countriesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlForm;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.DataGridView dgw1;
        private System.Windows.Forms.DataGridViewTextBoxColumn countriesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn slugDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSO2DataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource countriesBindingSource;
        private System.Windows.Forms.DataGridView dgw2;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn provinceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cityCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn latDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comfirmedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deathsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn recoveredDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource countryDataBindingSource;
        private System.Windows.Forms.Label LblTime;
    }
}