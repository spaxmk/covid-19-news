﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Markup;
using Covid_19_Geo_Updates;
using Newtonsoft.Json;
using RestSharp;

namespace Covid_19_Geo_Updates
{
    public partial class FormList : Form
    {
        public FormList()
        {
            InitializeComponent();
            LoadDataAsync();
            LblTime.Text = DateTime.Now.ToString("yyyy-MM-dd'T'");
            //LoadCountriesDataAsync();
            //dataManager = new DataManager();
            //this.DataContext = dataManager;
            //dgw2.Hide();
        }

        private async Task LoadDataAsync()
        {
            dgw1.Update();
            var dataList = await DataManager.ApiReadCountries();
            dgw1.DataSource = dataList;
        }


        private async Task LoadCountriesDataAsync()
        {
            //dgw2.Update();
            //var listData = DataManager.ApiReadData();
            //dgw2.DataSource = listData;

            //var client = new RestClient("https://api.covid19api.com/country/serbia?from=2020-10-02T00:00:00Z&to=2020-10-02T23:59:00Z");
            var client = new RestClient(CreateRestClientUrl("serbia"));
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var listData = JsonConvert.DeserializeObject<List<CountryData>>(response.Content);

            dgw2.Update();
            dgw2.DataSource = listData;
        }

        private string CreateRestClientUrl(string countries)
        {
            string apibaseUrl = @"https://api.covid19api.com/country/";
            //string date = DateTime.Now.ToString("yyyy-MM-dd'T'");
            string date = "2020-11-03T";
            string apiCountryData = apibaseUrl + countries + "?from=" + date + "00:00:00Z&to=" + date + "23:59:00Z";

            return apiCountryData;
        }

        //private async Task btnGet_Click_1(object sender, EventArgs e)
        //{
        //    await LoadCountriesDataAsync();
        //}

        private void dataManagerBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private async void btnGet_Click(object sender, EventArgs e)
        {
            await LoadCountriesDataAsync();
        }
    }
}
