﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Covid_19_Geo_Updates
{
    public static class DataManager 
    {
        //private const string countriesFileJSON = @"../../res/countries.json";
        //private const string apiUrl = @"https://api.covid19api.com/country/serbia?from=2020-10-01T00:00:00Z&to=2020-10-02T00:00:00Z";
        private const string apiBase = @"https://api.covid19api.com/country/";
        private const string apiCountries = @"https://api.covid19api.com/countries";

        //private CountriesList countriesList;

        //public DataManager()
        //{
        //    //LoadCountriesFromFile(countriesFileJSON);
        //    //ReadData();

        //    //thread = new Thread(new ThreadStart(ReadData));
        //    //thread.IsBackground = true;
        //    //thread.Start();
        //}

        //private void ReadData()
        //{
        //    while (true)
        //    {
        //        RefreshData();
        //        Thread.Sleep(100 * 60 * 1000); // Every 100min get new data 
        //    }

        //}

        //public event PropertyChangedEventHandler PropertyChanged;
        //protected virtual void OnPropertyChanged(string name)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        //}

        public static async Task<List<Countries>> ApiReadCountries()
        {
            try
            {
                var client = new RestClient(apiCountries);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var listCountries = JsonConvert.DeserializeObject<List<Countries>>(response.Content);
                return listCountries;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }

        public static async Task<List<CountryData>> ApiReadData()
        {
            try
            {
                var client = new RestClient("https://api.covid19api.com/country/serbia?from=2020-10-02T00:00:00Z&to=2020-10-02T23:59:00Z");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var listData = JsonConvert.DeserializeObject<List<CountryData>>(response.Content);

                return listData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }

        //public static string CreateApiUrl(string apiUrl)
        //{
        //    apiBase =  
        //}



        //Returns string data representation from API
        //private static string GetDataFromApi()
        //{
        //    string response = null;

        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.covid19api.com/country/serbia?from=2020-10-02T00:00:00Z&to=2020-10-02T23:59:00Z");
        //    request.AutomaticDecompression = DecompressionMethods.GZip;

        //    using (HttpWebResponse res = (HttpWebResponse)request.GetResponse())
        //    {
        //        using (Stream stream = res.GetResponseStream())
        //        {
        //            using (StreamReader reader = new StreamReader(stream))
        //            {
        //                response = reader.ReadToEnd();
        //            }
        //        }
        //    }

        //    request.Abort();

        //    return response;

        //}


        //private Countries countries;
        //private Thread thread;

        //public Countries Countries
        //{
        //    get { return countries; }
        //    set
        //    {
        //        countries = value;
        //        OnPropertyChanged("Countries");
        //    }
        //}

        //Refresh Data from Api
        //public void RefreshData()
        //{
        //    string response = GetDataFromApi();
        //    countries = JsonConvert.DeserializeObject<Countries>(response);
        //    //refreshTime = DateTime.Now.ToLongTimeString();
        //    FixData();
        //}

        //private void FixData()
        //{
        //    try
        //    {
        //        for (int i = 0; i < countries.myList.Count; i++)
        //        {
        //            CountryData obj = new CountryData();
        //            countries.myList = new List<CountryData>();
        //            obj.Country = countries.myList[i].Country.Substring(0, 10);
        //            obj.CountryCode = countries.myList[i].CountryCode.Substring(0, 10);
        //            obj.Province = countries.myList[i].Province.Substring(0, 10);
        //            obj.City = countries.myList[i].City.Substring(0, 10);
        //            obj.CityCode = countries.myList[i].CityCode.Substring(0, 10);
        //            obj.Lat = countries.myList[i].Lat.Substring(0, 10);
        //            obj.Lon = countries.myList[i].Lon.Substring(0, 10);
        //            obj.Comfirmed = countries.myList[i].Comfirmed;
        //            obj.Deaths = countries.myList[i].Deaths;
        //            obj.Recovered = countries.myList[i].Recovered;
        //            obj.Active = countries.myList[i].Active;
        //            obj.Date = countries.myList[i].Date.Substring(0, 10);

        //            countries.myList.Add(obj);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.ToString());
        //        throw;
        //    }

        //}

        //public void LoadMapData()
        //{
        //    LiveCharts.WinForms.GeoMap geoMap = new LiveCharts.WinForms.GeoMap();
        //    Dictionary<string, double> keyValues = new Dictionary<string, double>();
        //    keyValues["RS"] = 81285;
        //    keyValues["US"] = 75066;
        //    keyValues["IT"] = 74384;
        //    keyValues["ES"] = 56196;
        //    keyValues["IR"] = 29046;
        //    keyValues["FR"] = 25233;
        //    keyValues["UK"] = 9849;
        //    keyValues["CA"] = 3579;
        //    keyValues["PK"] = 1179;
        //    keyValues["IN"] = 719;

        //    for(int i=0; i<countries.myList.Count; i++)
        //    {

        //        string countryCovid = countries.myList[i].CountryCode;
        //        int comfirmedCovid = countries.myList[i].Comfirmed;
        //        keyValues[comfirmedCovid.ToString()] = comfirmedCovid;
        //    }

        //    geoMap.HeatMap = keyValues;

        //    geoMap.Source = @"../../res/World.xml";
        //    this.Controls.Add(geoMap);
        //    geoMap.Dock = DockStyle.Fill;
        //}
    }
}
