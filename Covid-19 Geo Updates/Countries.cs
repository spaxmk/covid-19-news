﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Covid_19_Geo_Updates
{
    public class Countries
    {
        public string Country { get; set; }
        public string Slug { get; set; }
        public string ISO2 { get; set; }

        //public IList<Countries> countries = new List<Countries>();
        public List<CountryData> myList { get; set; }
        
        public Countries()
        {

        }
        
        public Countries(string Country, string Slug, string ISO2)
        {
            this.Country = Country;
            this.Slug = Slug;
            this.ISO2 = ISO2;
        }
    }

    public class CountriesList
    {
        public IList<Countries> countries { get; set; }
    }

    public class CountryData
    {
        
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string Comfirmed { get; set; }
        public string Deaths { get; set; }
        public string Recovered { get; set; }
        public string Active { get; set; }
        public string Date { get; set; }

        public IList<Countries> countries = new List<Countries>();

        public CountryData()
        {

        }

        public CountryData(string Country, string CountryCode, string Province, string City, string CityCode, string Lat, string Lon, string Comfirmed, string Deaths, string Recovered, string Active, string Date)
        {
            this.Country = Country;
            this.CountryCode = CountryCode;
            this.Province = Province;
            this.City = City;
            this.CityCode = CityCode;
            this.Lat = Lat;
            this.Lon = Lon;
            this.Comfirmed = Comfirmed;
            this.Deaths = Deaths;
            this.Recovered = Recovered;
            this.Active = Active;
            this.Date = Date;
        }
    }
}
